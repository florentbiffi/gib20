let oldWidth = window.innerWidth;

const actions = {
    resize: (context) => {
        context.commit('SET_ISTOUCH', 'ontouchstart' in document.documentElement);

        if ('ontouchstart' in document.documentElement && oldWidth !== window.innerWidth) {
            oldWidth = window.innerWidth;
            context.commit('SET_MINHEIGHT', window.innerHeight);
        }

        context.commit('SET_WIDTH', window.innerWidth);
        context.commit('SET_HEIGHT', window.innerHeight);
    },
};

export default actions;
