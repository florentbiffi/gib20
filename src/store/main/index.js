import actions from './actions';
import mutations from './mutations';

const state = {
    isPageReady: false,
    navigationOpen: false,
    downloadPanelOpen: false,
    downloadPanelActive: 0,
    isSection: false,
    isTransition: false,
    currentNav: 0,
};

const store = {
    namespaced: true,
    state,
    actions,
    mutations,
};

export default store;
