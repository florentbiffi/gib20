export default function calculateAspectRatio(srcWidth, srcHeight, maxWidth, maxHeight, fit) {

    var ratio = [maxWidth / srcWidth, maxHeight / srcHeight ];
    ratio = (fit) ? Math.min(ratio[0], ratio[1]) : Math.max(ratio[0], ratio[1]);

    return { width: srcWidth*ratio, height: srcHeight*ratio };

}