export default class Loader {

    constructor() {
        this.images = [];
        this.loaded = 0;
    }

    add(url) {
        this.images.push(url);
        return this;
    }

    load() {
        this.images.forEach( (url) => {
            const image = new Image();

            image.onabort = () => {
                console.warn('Preload: image abort', url);
            };

            image.onerror = () => {
                console.warn('Preload: image error', url);
            };

            image.onload = this.onImageLoad.bind(this);
            image.src = url;
        });
    }

    onImageLoad() {
        this.loaded += 1;

        if (this.progress) {
            this.progress(this.loaded / this.images.length);
        }

        if ( this.loaded === this.images.length && this.complete ) {
            this.complete( this.images );
        }
    }

    set onComplete(callback) {
        this.complete = callback;
    }

    set onProgress(callback) {
        this.progress = callback;
    }

}
