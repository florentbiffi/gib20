export default function getCookie (cookieName) {
    var cookieString = document.cookie;

    if (cookieString.length !== 0) {
        var cookieValue = cookieString.match ( '(^| |;)[\\s]*' + cookieName + '=([^;]*)' );
        return (cookieValue) ? decodeURIComponent ( cookieValue[2] ) : null;
    }

    return null;
}
