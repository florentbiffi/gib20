// vue.config.js
const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')


module.exports = {
    transpileDependencies: [
        "@damienmortini/generalelectric-ib2020-webgl",
        "@damienmortini/generalelectric-ib2020-globe",
        "@damienmortini/generalelectric-ib2020-giants",
        "@damienmortini/three",
    ],
    configureWebpack: {
        plugins: [
            new CopyWebpackPlugin([
                { from: 'node_modules/@webcomponents/webcomponentsjs/', to: 'node_modules/@webcomponents/webcomponentsjs/' },
                { from: 'node_modules/@damienmortini/generalelectric-ib2020-globe/', to: 'node_modules/@damienmortini/generalelectric-ib2020-globe/' },
                { from: 'node_modules/@damienmortini/generalelectric-ib2020-giants/', to: 'node_modules/@damienmortini/generalelectric-ib2020-giants/' },
                { from: 'node_modules/three/examples/js/libs/draco/draco_decoder.wasm', to: 'node_modules/three/examples/js/libs/draco/draco_decoder.wasm' },
                { from: 'node_modules/three/examples/js/libs/draco/draco_wasm_wrapper.js', to: 'node_modules/three/examples/js/libs/draco/draco_wasm_wrapper.js' },
                { from: 'node_modules/three/examples/js/libs/basis/', to: 'node_modules/three/examples/js/libs/basis/' },
            ])
        ]
    },
    chainWebpack: config => {
        const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
        types.forEach(type => addStyleResource(config.module.rule('stylus').oneOf(type)))
    },
}

function addStyleResource (rule) {
    rule.use('style-resource')
        .loader('style-resources-loader')
        .options({
            patterns: [
                path.resolve(__dirname, './src/styles/settings/*.styl'),
            ]
        })
}
